package com.xperad.comet;

import android.app.Application;

import timber.log.Timber;

/**
 * Created by xperad on 2017/07/16.
 */

public class CometApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

    }
}
