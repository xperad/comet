package com.xperad.comet;

import android.support.v7.app.AppCompatActivity;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by xperad on 2017/07/16.
 */

public abstract class CometActivity extends AppCompatActivity {

    protected final CompositeDisposable disposable = new CompositeDisposable();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.clear();
    }

}
