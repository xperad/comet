package com.xperad.comet;

import android.os.Bundle;

public class MainActivity extends CometActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}